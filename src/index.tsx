import * as React from 'react'
import * as ReactDOM from 'react-dom'
import * as Navbar from 'react-bootstrap/lib/Navbar'
import * as Nav from 'react-bootstrap/lib/Nav'
import * as NavItem from 'react-bootstrap/lib/NavItem'
import * as NavDropdown from 'react-bootstrap/lib/NavDropdown'
import * as MenuItem from 'react-bootstrap/lib/MenuItem'
import * as Glyphicon from 'react-bootstrap/lib/Glyphicon'

import './styles.css'
import SideBar from './components/SideBar'

class AppComponent extends React.Component<{}, never> {
  render () {
    const logoStyle = {
      fontSize: '36px',
      transform: 'translateY(-10px)'
    }
    console.log(SideBar)
    return (
      <div>
        <Navbar inverse className={'navbar-fixed-top'}>
          <Navbar.Header>
            <Navbar.Brand>
              <a href='#home'><Glyphicon style={logoStyle} glyph='fire' /></a>
            </Navbar.Brand>
          </Navbar.Header>
          <Nav>
            <NavItem eventKey={1} href='#'>
              Link
            </NavItem>
            <NavItem eventKey={2} href='#'>
              Link
            </NavItem>
            <NavDropdown eventKey={3} title='Dropdown' id='basic-nav-dropdown'>
              <MenuItem eventKey={3.1}>Action</MenuItem>
              <MenuItem eventKey={3.2}>Another action</MenuItem>
              <MenuItem eventKey={3.3}>Something else here</MenuItem>
              <MenuItem divider />
              <MenuItem eventKey={3.4}>Separated link</MenuItem>
            </NavDropdown>
          </Nav>
        </Navbar>
        <img src='http://circlebq.com/upload/medialibrary/d7c/d7c9949abaea583677d6f3beb4fa4a87.jpg' className={'background-image large'} />
      </div>
    )
  }
}

ReactDOM.render(<AppComponent />, document.getElementById('app'))
