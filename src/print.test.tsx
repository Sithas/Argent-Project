import { default as printMe } from './print'

test('adds 1 + 2 to equal 3', () => {
  expect(printMe(1, 2)).toBe(3)
})
